﻿package {

	import flash.display.MovieClip;
	import network_pkg.*;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.events.MouseEvent;


	public class Network extends MovieClip {

		var arrayPoints: Array = new Array();
		private var timerAleatoire: Timer = new Timer(2000);

		var i: int = 0;
		var sonCloche: Sound = new(ding);
		var sonBackground: Sound = new(space);
		var sonClick: Sound = new(click);
		var volumeSon: SoundTransform = new SoundTransform(0.25, 0);

		public function Network() {
			// constructor code
			this.timerAleatoire.delay = Math.floor(Math.random() * 5000);
			this.timerAleatoire.start();
			this.timerAleatoire.addEventListener(TimerEvent.TIMER, this.creerPoint);
			this.stage.addEventListener(MouseEvent.CLICK, this.creerPointManuel);

			this.sonBackground.play(0, 0, this.volumeSon);
		}

		private function creerPoint(evenement: TimerEvent): void {
			var positionX = Math.floor(Math.random() * this.stage.stageWidth);
			var positionY = Math.floor(Math.random() * this.stage.stageHeight);

			trace(positionX, positionY);

			//Code de http://stackoverflow.com/questions/24050875/random-hexidecimal-color-generator-in-as3
			var red: int = Math.floor(Math.random() * 255);
			var green: int = Math.floor(Math.random() * 255);
			var blue: int = Math.floor(Math.random() * 255);
			var color: int = red << 16 | green << 8 | blue;

			var diametre: int = Math.floor(Math.random() * 10);

			var point: Point = new Point(this, positionX, positionY, color, diametre);

			this.arrayPoints.push([point.x, point.y, color, diametre]);

			this.dessinerLigne(this.i);

			this.i = this.i + 1;

			this.timerAleatoire.delay = Math.floor(Math.random() * 5000);

			this.sonCloche.play(0, 0, this.volumeSon);
		}

		private function creerPointManuel(evenement: MouseEvent): void {
			var positionX = evenement.target.mouseX;
			var positionY = evenement.target.mouseY;

			trace(positionX, positionY);

			//Code de http://stackoverflow.com/questions/24050875/random-hexidecimal-color-generator-in-as3
			var red: int = Math.floor(Math.random() * 255);
			var green: int = Math.floor(Math.random() * 255);
			var blue: int = Math.floor(Math.random() * 255);
			var color: int = red << 16 | green << 8 | blue;

			var diametre: int = Math.floor(Math.random() * 10);
			trace(undefined);

			var point: Point = new Point(this, positionX, positionY, color, diametre);

			this.arrayPoints.push([point.x, point.y, color, diametre]);

			this.dessinerLigne(this.i);

			this.i = this.i + 1;

			this.sonClick.play(0, 0, this.volumeSon);
		}

		private function dessinerLigne(ptPresent): void {
			var precedent: int = ptPresent - 1;

			trace("Point préc: " + precedent);
			trace("Point Actuel " + ptPresent);

			if (ptPresent >  0) {
				var positionIniX: int = this.arrayPoints[precedent][0];
				var positionIniY: int = this.arrayPoints[precedent][1];

				var positionFinX: int = this.arrayPoints[ptPresent][0];
				var positionFinY: int = this.arrayPoints[ptPresent][1];

				var deltaFinX: int = positionFinX - positionIniX;
				var deltaFinY: int = positionFinY - positionIniY;

				var couleur: int = this.arrayPoints[ptPresent][2];

				var ligne: Ligne = new Ligne(this, positionIniX, positionIniY, deltaFinX, deltaFinY, couleur);
			}
		}
	}

}