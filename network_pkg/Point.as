﻿package network_pkg {
	import flash.display.Shape;

	public class Point extends ObjetVisible {

		public function Point(refScenario, positionX, positionY, couleur, diametre) {
			// constructor code
			super(refScenario, positionX, positionY);
			this.dessinerPoint(couleur, diametre);
		}
		private function dessinerPoint(couleur, diametre) {
			var pointCentral: int = diametre / 2;

			this.graphics.beginFill(couleur);
			this.graphics.drawEllipse(-pointCentral, -pointCentral, diametre, diametre);

			this.graphics.endFill();
		}
	}

}