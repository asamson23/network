﻿package network_pkg {
	import flash.display.Graphics;

	public class Ligne extends ObjetVisible {

		public function Ligne(refScenario, positionX, positionY, pointFinX, pointFinY, couleur) {
			// constructor code
			super(refScenario, positionX, positionY);
			this.dessinerLigne(pointFinX, pointFinY, couleur);
		}

		private function dessinerLigne(pointFinX, pointFinY, couleur): void {
			this.graphics.moveTo(0, 0);
			this.graphics.beginFill(0xffffff);
			this.graphics.lineStyle(1, couleur);
			this.graphics.lineTo(pointFinX, pointFinY);
			this.graphics.lineTo(0, 0);
			this.graphics.endFill();
		}

	}

}