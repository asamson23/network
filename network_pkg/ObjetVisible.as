﻿package network_pkg {
	import flash.events.*;
	import flash.display.MovieClip;
	public class ObjetVisible extends MovieClip {
		protected var monClip_mc: MovieClip = null;
		protected var leScenario = null;

		public function ObjetVisible(scenario, posX, posY) {
			this.leScenario = scenario;
			this.leScenario.addChild(this);
			this.x = posX;
			this.y = posY;
		}

		public function retournerMonClip_mc() {
			return this;
		}
		public function arretObjetVisible(): void {
			this.leScenario.removeChild(this);
		}
	}
}